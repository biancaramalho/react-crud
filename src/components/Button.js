import React from 'react';
import PropTypes from 'prop-types'

const Button = ({ onClick }) => {

    return (
        <div>
            <button className='button-add'
            onClick={onClick}> Add task </button>  
        </div>
    )
}

Button.propTypes = {
    onClick: PropTypes.func,
  }

export default Button
