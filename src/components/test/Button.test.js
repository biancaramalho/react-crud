import React from 'react';
import 'regenerator-runtime/runtime';
import { render, screen } from '@testing-library/react';
import Button from '../Button';
import '@testing-library/jest-dom';

render(<Button />);
describe('Button component', () => {
        it('should have a Add task', () => {
            const buttonAdd = screen.getByText('Add task')
            expect(buttonAdd).toBeInTheDocument();
        });
})
