import React from 'react';
import { FaTimes } from 'react-icons/fa'

const Task = ({ task, onDelete, onToggle }) => {
    return (
        <div className={`task ${task.check ? 'check' : ''}`} 
        onDoubleClick={() => onToggle(task.id)}>

            <h2>{task.description} <FaTimes style={{ color: 'red'}} 
            onClick={() => onDelete(task.id)} /> </h2>
            <p>{task.date}</p>
        </div>
    )
}

export default Task
