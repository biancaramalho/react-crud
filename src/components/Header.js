import React from 'react';
import Button from './Button'

const Header = ({ onAdd }) => {
    return (
        <header className='header'>
            <h1>CRUD Tasks</h1>
            <Button onClick={onAdd} />                    
        </header>
    )
}

export default Header
