import React from 'react';
import { useState } from 'react'

const AddTask = ({ onAdd }) => {
    const [description, setDescription] = useState('')
    const [date, setDate] = useState('')
    const [check, setCheck] = useState(false)

    const onSubmit = (e) => {
        e.preventDefault()

        if(!description) {
            alert('Please, a task must be added')
            return
        }

        onAdd({ description, date, check })

        setDescription('')
        setDate('')
        setCheck(false)
    }
    
    return (
        <form className='add-form' onSubmit={onSubmit}>
            <div className='form-control'>
                <label>Task</label>
                <input type='text' placeholder='Add task'
                value={description} onChange={(e) => setDescription(e.target.value)} />
            </div>

            <div className='form-control'>
                <label>Date</label>
                <input type='text' placeholder='Add date'
                value={date} onChange={(e) => setDate(e.target.value)} />
            </div> 

            <div className='form-control form-control-check'>
                <label>Reminder</label>
                <input type='checkbox'
                checked={check}
                value={check} onChange={(e) => setCheck(e.currentTarget.checked)} />

            <input type='submit' value='save' className='btn button-block'/>
            </div>             
        </form>
    )
}

export default AddTask
