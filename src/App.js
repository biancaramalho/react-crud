import React from 'react';
import { useState, useEffect } from 'react'
import Header from './components/Header'
import Tasks from './components/Tasks'
import AddTask from './components/AddTask'

function App() {
  const [ showAddTask, setShowAddTask ] = useState(false)

  const [ tasks, setTasks ] = useState([])

  useEffect(() => {
    const getTask = async () => {
      const taskServer = await fetchTask()
      setTasks(taskServer)
    }

    getTask()
  }, [])

// Fetch tasks
const fetchTask = async () => {
  const res = await fetch('http://localhost:5000/tasks')
  const data = await res.json()

  return data
}

// Add Task
const addTask = async (task) => {
  const res = await fetch('http://localhost:5000/tasks/', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(task)
  })

  const data = await res.json()
  setTasks([...tasks, data])
}

// Update each task
const updateTask = async (id) => {
  const res = await fetch(`http://localhost:5000/tasks/${id}`)
  const data = await res.json()

  return data
}

// Delete Task
const deleteTask = async (id) => { 
  await fetch(`http://localhost:5000/tasks/${id}`, {
      method: 'DELETE',
    })

  setTasks(tasks.filter((task) => task.id !== id))
}

// Reminder-Check 
const toggleReminder = async (id) => {
  const taskToggle = await updateTask(id)
  const upTask = {...taskToggle, check: !taskToggle.check}

  const res = await fetch(`http://localhost:5000/tasks/${id}`, {
    method: 'PUT',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(upTask)
  })

  const data = await res.json()

      setTasks(tasks.map((task) => task.id === id ? {
        ...task, check: data.check
      } : task
    )
  )
}

  return (
    <div className="container">
      <Header onAdd={() => setShowAddTask(!showAddTask)}/>
      {showAddTask && <AddTask onAdd={addTask} />}
      {tasks.length > 0 ? <Tasks 
        tasks={tasks} 
        onDelete={deleteTask} 
        onToggle={toggleReminder} /> : 'No tasks for now.'}
      </div>
  );
}

export default App;
