### Create a small frontend CRUD application using React. My tasks step-by-step.

### What should this application do?
It should be as minimal as possible, doing only what is necessary for demonstration purposes. It should be composed by a single set of CRUD operations.

- [x] Basic concepts: component lifecycle, composition, difference between using state vs props, etc;
- [x] Styling with:  
  - [x] Flexbox.

- [ ] Use a testing framework (jest recommended);
- [ ] Implement unit tests for at least one component;
- [x] Consuming an API: demonstrate knowledge of how and when to fetch data properly.